﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace Products_Management.BL
{
    class CLS_CUSTOMERS
    {
        public void ADD_CUSTOMER(string First_Name, string Last_Name, string Tel, string Email, byte[] img,string Criterion)
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DAL.Open();
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@First_Name", SqlDbType.VarChar,25);
            param[1] = new SqlParameter("@Last_Name", SqlDbType.VarChar, 25);
            param[2] = new SqlParameter("@Tel", SqlDbType.NChar, 15);
            param[3] = new SqlParameter("@Email", SqlDbType.VarChar,25);
            param[4] = new SqlParameter("@Picture", SqlDbType.Image);
            param[5] = new SqlParameter("@Criterion", SqlDbType.VarChar, 50);

            param[0].Value = First_Name;
            param[1].Value = Last_Name;
            param[2].Value = Tel;
            param[3].Value = Email;
            param[4].Value = img;
            param[5].Value = Criterion;
            DAL.ExecuteData("ADD_CUSTOMER", param);
            DAL.Close();
        }
        public void EDIT_CUSTOMER(int ID,string First_Name, string Last_Name, string Tel, string Email, byte[] img, string Criterion)
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DAL.Open();
            SqlParameter[] param = new SqlParameter[7];
            param[0]=new SqlParameter("@ID", SqlDbType.Int);
            param[1] = new SqlParameter("@First_Name", SqlDbType.VarChar, 25);
            param[2] = new SqlParameter("@Last_Name", SqlDbType.VarChar, 25);
            param[3] = new SqlParameter("@Tel", SqlDbType.NChar, 15);
            param[4] = new SqlParameter("@Email", SqlDbType.VarChar, 25);
            param[5] = new SqlParameter("@Picture", SqlDbType.Image);
            param[6] = new SqlParameter("@Criterion", SqlDbType.VarChar, 50);
            param[0].Value = ID;
            param[1].Value = First_Name;
            param[2].Value = Last_Name;
            param[3].Value = Tel;
            param[4].Value = Email;
            param[5].Value = img;
            param[6].Value = Criterion;
            DAL.ExecuteData("EDIT_CUSTOMER", param);
            DAL.Close();
        }

        public DataTable GET_ALL_CUSTOMERS()
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DAL.Open();
            DataTable dt = new DataTable();

            dt = DAL.SelectData("GET_ALL_CUSTOMERS", null);
            DAL.Close();
            return dt;
        }
        public void DeleteCustomer(int ID)
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DAL.Open();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ID", SqlDbType.Int);
            param[0].Value = ID;
            DAL.ExecuteData("DeleteCustomer", param);
            DAL.Close();

        }
        public DataTable SEARCH_CUSTOMERS(string first,string last,string criterion)
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DAL.Open();
            DataTable dt = new DataTable();
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@FIRST", SqlDbType.VarChar, 25);
            param[0].Value = first;
            param[1] = new SqlParameter("@LAST", SqlDbType.VarChar, 25);
            param[1].Value = last;
            param[2] = new SqlParameter("@CRITERION", SqlDbType.VarChar, 50);
            param[2].Value = criterion;

            dt = DAL.SelectData("SEARCH_CUSTOMERS", param);
            DAL.Close();
            return dt;
        }
    }
}
