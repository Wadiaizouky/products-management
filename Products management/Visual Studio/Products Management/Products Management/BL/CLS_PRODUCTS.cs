﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace Products_Management.BL
{
    class CLS_PRODUCTS
    {
        public DataTable Get_All_Categories()
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DAL.Open();
            DataTable dt = new DataTable();

            dt = DAL.SelectData("Get_All_Categories", null);
            DAL.Close();
            return dt;
        }
        public void Add_Product(int ID_cat, string Label_product, string ID_product, int qte,
            string price, byte[] img)
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DAL.Open();
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@ID_CAT", SqlDbType.Int);
            param[1] = new SqlParameter("@ID_PRODUCT", SqlDbType.VarChar, 30);
            param[2] = new SqlParameter("@Label", SqlDbType.VarChar, 30);
            param[3] = new SqlParameter("@Qte", SqlDbType.Int);
            param[4] = new SqlParameter("@Price", SqlDbType.VarChar, 50);
            param[5] = new SqlParameter("@Img", SqlDbType.Image);
            param[0].Value = ID_cat;
            param[1].Value = ID_product;
            param[2].Value = Label_product;
            param[3].Value = qte;
            param[4].Value = price;
            param[5].Value = img;
            DAL.ExecuteData("ADD_PRODUCT", param);
            DAL.Close();
        }
        public DataTable VerifyProductID(string ID)
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DAL.Open();
            DataTable dt = new DataTable();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ID", SqlDbType.VarChar, 50);
            param[0].Value = ID;
            dt = DAL.SelectData("VerifyProductID", param);
            DAL.Close();
            return dt;
        }
        public DataTable Get_All_Products()
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DAL.Open();
            DataTable dt = new DataTable();

            dt = DAL.SelectData("Get_All_Products", null);
            DAL.Close();
            return dt;
        }
        public DataTable SearchProduct(string ID)
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DAL.Open();
            DataTable dt = new DataTable();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ID", SqlDbType.VarChar, 50);
            param[0].Value = ID;
            dt = DAL.SelectData("SearchProduct", param);
            DAL.Close();
            return dt;
        }
        public void DeleteProduct(string ID)
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DAL.Open();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ID", SqlDbType.VarChar, 50);
            param[0].Value = ID;
            DAL.ExecuteData("DeleteProduct", param);
            DAL.Close();

        }
        public DataTable GET_ID1(string ID)
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DAL.Open();
            DataTable dt = new DataTable();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ID", SqlDbType.VarChar, 30);
            param[0].Value = ID;
            dt = DAL.SelectData("GET_ID1", param);
            DAL.Close();
            return dt;
        }
        public DataTable GET_CATEGORY(string CAT)
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DAL.Open();
            DataTable dt = new DataTable();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@CATEGORY_NAME", SqlDbType.VarChar, 25);
            param[0].Value = CAT;
            dt = DAL.SelectData("GET_CATEGORY", param);
            DAL.Close();
            return dt;
        }
        public DataTable GET_IMAGE_PRODUCT(string ID)
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DAL.Open();
            DataTable dt = new DataTable();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ID", SqlDbType.VarChar, 50);
            param[0].Value = ID;
            dt = DAL.SelectData("GET_IMAGE_PRODUCT", param);
            DAL.Close();
            return dt;
        }
        public void UPDATE_PRODUCT(int ID_cat, string Label_product, string ID_product, int qte,
            string price, byte[] img)
        {
            DAL.DataAccessLayer DAL = new DAL.DataAccessLayer();
            DAL.Open();
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@ID_CAT", SqlDbType.Int);
            param[1] = new SqlParameter("@ID_PRODUCT", SqlDbType.VarChar, 30);
            param[2] = new SqlParameter("@Label", SqlDbType.VarChar, 30);
            param[3] = new SqlParameter("@Qte", SqlDbType.Int);
            param[4] = new SqlParameter("@Price", SqlDbType.VarChar, 50);
            param[5] = new SqlParameter("@Img", SqlDbType.Image);
            param[0].Value = ID_cat;
            param[1].Value = ID_product;
            param[2].Value = Label_product;
            param[3].Value = qte;
            param[4].Value = price;
            param[5].Value = img;
            DAL.ExecuteData("UPDATE_PRODUCT", param);
            DAL.Close();
        }
    }
}
