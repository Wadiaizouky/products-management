﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
namespace Products_Management.DAL
{
    class DataAccessLayer
    {
        SqlConnection sqlConnection;
        //This constructor initialize the connection object
        public DataAccessLayer()
        {
            sqlConnection = new SqlConnection(@"Server=.\SQLEXPRESS; Database=Product_DB; Integrated Security=true");

        }
        //Method to open the connection
        public void Open()
        {
            if (sqlConnection.State != ConnectionState.Open)
                sqlConnection.Open();
        }
        //Method to close the connection
        public void Close()
        {
            if (sqlConnection.State != ConnectionState.Closed)
                sqlConnection.Close();
        }
        //Method to read data from database
        public DataTable SelectData(string stored_procedure,SqlParameter[] param)
        {
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandType = CommandType.StoredProcedure;
            sqlcmd.CommandText = stored_procedure;
            sqlcmd.Connection = sqlConnection;
            if (param != null)
            {
                for (int i = 0; i < param.Length; i++)
                {
                    sqlcmd.Parameters.Add(param[i]);
                }
            }
                SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            
            
        }
        //method to Insert, Update, Delete from DataBase
        public void ExecuteData(string stored_procedure,SqlParameter[] param)
        {
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandType = CommandType.StoredProcedure;
            sqlcmd.CommandText = stored_procedure;
            sqlcmd.Connection = sqlConnection;
            if (param != null)
            {
               
                    sqlcmd.Parameters.AddRange(param);    
            }
            sqlcmd.ExecuteNonQuery();
        }
    }
}
