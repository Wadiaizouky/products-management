﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
namespace Products_Management.PL
{
    public partial class FRM_ADD_PRODUCTS : Form
    {
        BL.CLS_PRODUCTS products = new BL.CLS_PRODUCTS();
        FRM_PRODUCTS frm = new FRM_PRODUCTS();
        public string state = "add";
        public FRM_ADD_PRODUCTS()
        {
            InitializeComponent();
            this.BackColor = Color.FromArgb(45, 45, 45);
            cmbCategories.DataSource = products.Get_All_Categories();
            cmbCategories.DisplayMember = "DESCRIBTION_CAT";
            cmbCategories.ValueMember = "ID_CAT";
            


        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
                    }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Image type |*.JPG; *.JPEG; *.PNG; *.GIF; *.BMP";
            if(openFileDialog1.ShowDialog()==DialogResult.OK)
            pBox.Image = Image.FromFile(openFileDialog1.FileName);
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (state == "add")
            {
                MemoryStream ms = new MemoryStream();
                pBox.Image.Save(ms, pBox.Image.RawFormat);
                byte[] byteImage = ms.ToArray();
                products.Add_Product(Convert.ToInt32(cmbCategories.SelectedValue), txtDec.Text, txtRef.Text, Convert.ToInt32(txtQte.Text), txtPrice.Text, byteImage);
                MessageBox.Show("The product is added successfully", "Add", MessageBoxButtons.OK, MessageBoxIcon.Information);
                FRM_PRODUCTS.getProductForm.dataGridView1.DataSource = products.Get_All_Products();
            }
            else
            {
                MemoryStream ms = new MemoryStream();
                pBox.Image.Save(ms, pBox.Image.RawFormat);
                byte[] byteImage = ms.ToArray();
                products.UPDATE_PRODUCT(Convert.ToInt32(cmbCategories.SelectedValue), txtDec.Text, txtRef.Text, Convert.ToInt32(txtQte.Text), txtPrice.Text, byteImage);
                MessageBox.Show("The product is updated successfully", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                FRM_PRODUCTS.getProductForm.dataGridView1.DataSource = products.Get_All_Products();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtRef_Validated(object sender, EventArgs e)
        {if (state == "add")
            {
                DataTable dt = new DataTable();
                dt = products.VerifyProductID(txtRef.Text);
                if (dt.Rows.Count > 0)
                {
                    MessageBox.Show("The ID is already exists", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtRef.Focus();
                    txtRef.SelectionStart = 0;
                    txtRef.SelectionLength = txtRef.TextLength;
                }
            }
        }

        private void cmbCategories_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtRef_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDec_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtQte_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {

        }

        private void pBox_Click(object sender, EventArgs e)
        {

        }
    }
}
