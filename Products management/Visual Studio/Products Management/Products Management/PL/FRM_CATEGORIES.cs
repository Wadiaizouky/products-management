﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
namespace Products_Management.PL
{
    public partial class FRM_CATEGORIES : Form
    {
        SqlConnection sqlco = new SqlConnection(@"Server =.\SQLEXPRESS; Database=Product_DB; Integrated Security = true");
        SqlDataAdapter dt;
        DataTable Dat = new DataTable();
        BindingManagerBase b;
        SqlCommandBuilder cb;

        public FRM_CATEGORIES()
        {
            InitializeComponent();
            groupBox1.BackColor = Color.FromArgb(45, 45, 45);
            groupBox2.BackColor= Color.FromArgb(45, 45, 45);
            groupBox3.BackColor = Color.FromArgb(45, 45, 45);
            this.BackColor = Color.FromArgb(45, 45, 45);
            String comend = "select ID_CAT as 'رقم الصنف',DESCRIBTION_CAT as 'الصنف' from CATEGORIES";
            dt = new SqlDataAdapter(comend, sqlco);
            dt.Fill(Dat);
            dataGridView1.DataSource = Dat;
            //DataBinding
            textBox1.DataBindings.Add("text",Dat, "رقم الصنف");
            textBox2.DataBindings.Add("text", Dat, "الصنف");
            b = this.BindingContext[Dat];
            lblPosition.Text = (b.Position + 1) + " / " + b.Count;
            this.BackColor = Color.FromArgb(45, 45, 45);
        }

        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            
                b.EndCurrentEdit();
                cb = new SqlCommandBuilder(dt);
                dt.Update(Dat);
            lblPosition.Text = (b.Position + 1) + " / " + b.Count;
            MessageBox.Show("تم التعديل بنجاح", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Information);
           
            
        }

        private void button8_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("هل تريد تأكيد عملية الحذف ؟", "",
            MessageBoxButtons.OKCancel,
            MessageBoxIcon.Question);
           // if (result == DialogResult.Yes)
            //{
                b.RemoveAt(b.Position);
                b.EndCurrentEdit();
                cb = new SqlCommandBuilder(dt);
                dt.Update(Dat);
                MessageBox.Show("تمت الحذف بنجاح", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            lblPosition.Text = (b.Position + 1) + " / " + b.Count;
            //}
        }

        private void button9_Click(object sender, EventArgs e)
        {
            b.EndCurrentEdit();
            cb = new SqlCommandBuilder(dt);
            dt.Update(Dat);
            MessageBox.Show("تمت الاضافة بنجاح","Add",MessageBoxButtons.OK,MessageBoxIcon.Information);
            dataGridView1.CurrentCell.Style.BackColor = Color.Green;

            button5.Enabled = true;
            button9.Enabled = false;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            b.Position++;
            lblPosition.Text = (b.Position + 1) + " / " + b.Count;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            b.Position--;
            lblPosition.Text = (b.Position + 1) + " / " + b.Count;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            b.Position = b.Count;
            lblPosition.Text = (b.Position + 1) + " / " + b.Count;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            b.Position = 0 ;
            lblPosition.Text = (b.Position + 1) + " / " + b.Count;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //int i = Convert.ToInt32(Dat.Rows[Dat.Rows.Count][0]) + 1;
            b.AddNew();
            button5.Enabled = false;
            button9.Enabled = true;
            //int i = Convert.ToInt32(Dat.Rows[Dat.Rows.Count-1][0])+1;
            int i = b.Count;
            textBox1.Text = i.ToString();
            textBox2.Focus();

            // dataGridView1.CurrentCell.Style.BackColor = Color.Green;
            lblPosition.Text = (b.Position + 1) + " / " + b.Count;
            
        }

        private void button13_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void FRM_CATEGORIES_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_LocationChanged(object sender, EventArgs e)
        {
          
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            RPT.rbt_all_categories rbt = new RPT.rbt_all_categories();
            RPT.FRM_RPT_PRODUCT frm = new RPT.FRM_RPT_PRODUCT();
            rbt.Refresh();
            frm.crystalReportViewer1.ReportSource = rbt;
            frm.ShowDialog();

        }

        private void button11_Click(object sender, EventArgs e)
        {
            RPT.rbt_single_category rpt = new RPT.rbt_single_category();
            RPT.FRM_RPT_PRODUCT frm = new RPT.FRM_RPT_PRODUCT();
            rpt.SetParameterValue("@ID", dataGridView1.CurrentRow.Cells[0].Value.ToString());
            frm.crystalReportViewer1.ReportSource = rpt;
            frm.ShowDialog();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            RPT.rbt_all_categories rpt = new RPT.rbt_all_categories();
            rpt.Refresh();
            ExportOptions eo = new ExportOptions();
            DiskFileDestinationOptions dfdo = new DiskFileDestinationOptions();
            PdfFormatOptions pfo = new PdfFormatOptions();
            dfdo.DiskFileName = @"F:\file2.pdf";
            eo = rpt.ExportOptions;
            eo.ExportDestinationType = ExportDestinationType.DiskFile;
            eo.ExportFormatType = ExportFormatType.PortableDocFormat;
            eo.ExportFormatOptions = pfo;
            eo.ExportDestinationOptions = dfdo;
            rpt.Export();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            RPT.rbt_single_category rpt = new RPT.rbt_single_category();
            rpt.SetParameterValue("@ID", this.dataGridView1.CurrentRow.Cells[0].Value.ToString());
            ExportOptions eo = new ExportOptions();
            DiskFileDestinationOptions dfdo = new DiskFileDestinationOptions();
            PdfFormatOptions pfo = new PdfFormatOptions();
            dfdo.DiskFileName = @"F:\file1.pdf";
            eo = rpt.ExportOptions;
            eo.ExportDestinationType = ExportDestinationType.DiskFile;
            eo.ExportFormatType = ExportFormatType.PortableDocFormat;
            eo.ExportFormatOptions = pfo;
            eo.ExportDestinationOptions = dfdo;
            rpt.Export();
        }
    }
}
