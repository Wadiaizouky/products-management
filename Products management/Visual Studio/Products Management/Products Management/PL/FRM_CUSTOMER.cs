﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
namespace Products_Management.PL
{
    public partial class FRM_CUSTOMER : Form
    {
        BL.CLS_CUSTOMERS customer = new BL.CLS_CUSTOMERS();
        int position;
        public FRM_CUSTOMER()
        {
            InitializeComponent();
            this.BackColor = Color.FromArgb(45, 45, 45);
            dataGridView1.DataSource = customer.GET_ALL_CUSTOMERS();
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[5].Visible = false;
            label6.Text = (position + 1) + "/" + customer.GET_ALL_CUSTOMERS().Rows.Count;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (position == 0)
            {
                MessageBox.Show("هذا هو أول عنصر");
                return;
            }
            position--;
            Navigate(position);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Filter = "Image type |*.JPG; *.JPEG; *.PNG; *.GIF; *.BMP";
            if (op.ShowDialog() == DialogResult.OK)
                pictureBox1.Image = Image.FromFile(op.FileName);
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (textBox3.Text != ""&&textBox2.Text!="")
            {
                byte[] b;
                try
                {
                    if (pictureBox1.Image == null)
                    {
                        b = new byte[0];
                        customer.ADD_CUSTOMER(textBox3.Text, textBox2.Text, textBox4.Text, textBox5.Text, b, "without_image");
                        MessageBox.Show("تمت الإضافة بنجاح", "الإضافة", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dataGridView1.DataSource = customer.GET_ALL_CUSTOMERS();

                    }
                    MemoryStream memo = new MemoryStream();
                    pictureBox1.Image.Save(memo, pictureBox1.Image.RawFormat);
                    b = memo.ToArray();
                    customer.ADD_CUSTOMER(textBox3.Text, textBox2.Text, textBox4.Text, textBox5.Text, b, "with_image");
                    MessageBox.Show("تمت الإضافة بنجاح", "الإضافة", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dataGridView1.DataSource = customer.GET_ALL_CUSTOMERS();

                }
                catch
                {
                    return;
                }
                finally
                {
                    button7.Enabled = false;
                    button6.Enabled = true;
                    label6.Text = (position + 1) + "/" + customer.GET_ALL_CUSTOMERS().Rows.Count;

                }
            }
            else
            {
                button7.Enabled = false;
                button6.Enabled = true;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            button7.Enabled = true;
            button6.Enabled = false;
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            pictureBox1.Image = null;
            textBox3.Focus();

        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            try {
                pictureBox1.Image = null;
                textBox3.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                textBox2.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                textBox4.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                textBox5.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
                byte[] b = (byte[])dataGridView1.CurrentRow.Cells[5].Value;
                MemoryStream memo = new MemoryStream(b);
                pictureBox1.Image = Image.FromStream(memo);
                label6.Text = (position + 1) + "/" + customer.GET_ALL_CUSTOMERS().Rows.Count;

            }
            catch
            {
                return;
            }
            
        }

        private void button9_Click(object sender, EventArgs e)
        {
            byte[] b;
            try
            {
                if (pictureBox1.Image == null)
                {
                    b = new byte[0];
                    customer.EDIT_CUSTOMER(Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value), textBox3.Text, textBox2.Text, textBox4.Text, textBox5.Text, b, "without_image");
                    MessageBox.Show("تم التعديل بنجاح", "التعديل", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dataGridView1.DataSource = customer.GET_ALL_CUSTOMERS();

                }
                MemoryStream memo = new MemoryStream();
                pictureBox1.Image.Save(memo, pictureBox1.Image.RawFormat);
                b = memo.ToArray();
                customer.EDIT_CUSTOMER(Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value),textBox3.Text, textBox2.Text, textBox4.Text, textBox5.Text, b, "with_image");
                MessageBox.Show("تم التعديل بنجاح", "التعديل", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dataGridView1.DataSource = customer.GET_ALL_CUSTOMERS();

            }
            catch
            {
                return;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try {
                if (MessageBox.Show("هل تريد بالتأكيد حذف هذا العميل", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    customer.DeleteCustomer(Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value));
                    MessageBox.Show("تم الحذف بنجاح", "الحذف", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dataGridView1.DataSource = customer.GET_ALL_CUSTOMERS();

                }
            }
            catch
            {
                return;
            }
            finally
            {
                label6.Text = (position + 1) + "/" + customer.GET_ALL_CUSTOMERS().Rows.Count;

            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(comboBox1.SelectedIndex==0)
            {
                dataGridView1.DataSource = customer.SEARCH_CUSTOMERS(textBox1.Text, "dsf", "FIRST");
            }
            else if (comboBox1.SelectedIndex == 1)
            {
                dataGridView1.DataSource = customer.SEARCH_CUSTOMERS("df", textBox1.Text, "LAST");
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = customer.GET_ALL_CUSTOMERS();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            position = customer.GET_ALL_CUSTOMERS().Rows.Count - 1;
            Navigate(position);
        }
        void Navigate (int index)
        { try { pictureBox1.Image = null;
                DataTable dt = customer.GET_ALL_CUSTOMERS();
                textBox3.Text = dt.Rows[index][1].ToString();
                textBox2.Text = dt.Rows[index][2].ToString();
                textBox4.Text = dt.Rows[index][3].ToString();
                textBox5.Text = dt.Rows[index][4].ToString();
                if (dt.Rows[index][5] != null)
                {
                    byte[] b = (byte[])dt.Rows[index][5];
                    MemoryStream memo = new MemoryStream(b);
                    pictureBox1.Image = Image.FromStream(memo);
                }
                
            }
            catch
            {
                return;
            }
            finally
            {
                label6.Text = (position + 1) + "/" + customer.GET_ALL_CUSTOMERS().Rows.Count;

            }


        }

        private void button4_Click(object sender, EventArgs e)
        {
            Navigate(0);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (position == customer.GET_ALL_CUSTOMERS().Rows.Count - 1)
            {
                MessageBox.Show("هذا هو اخر عنصر");
                return;
            }
            position++;
            Navigate(position);
        }
    }
}
