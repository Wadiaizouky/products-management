﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Products_Management.PL
{
    public partial class FRM_Login : Form
    {
        BL.CLS_LOGIN log = new BL.CLS_LOGIN();
        public FRM_Login()
        {
            InitializeComponent();
            this.BackColor = Color.FromArgb(45, 45, 45);
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = log.Login(txtID.Text, txtPWD.Text);
            if (dt.Rows.Count > 0)
            {
                FRM_Main.getMainForm.makeBackupToolStripMenuItem.Enabled = true;
                FRM_Main.getMainForm.restoreBackupToolStripMenuItem.Enabled = true;
                FRM_Main.getMainForm.productsToolStripMenuItem.Enabled = true;
                FRM_Main.getMainForm.customersToolStripMenuItem.Enabled = true;
                FRM_Main.getMainForm.usersToolStripMenuItem.Enabled = true;
                this.Close();

            }
            else
                MessageBox.Show("Login failed !");
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FRM_Login_Load(object sender, EventArgs e)
        {

        }
    }
}
