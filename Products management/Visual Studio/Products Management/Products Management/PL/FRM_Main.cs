﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Products_Management.PL
{
    public partial class FRM_Main : Form
    {
        private static FRM_Main frm;
        static void frm_FormClosed(object sender,FormClosedEventArgs e)
        {
            frm = null;
        }
        public static FRM_Main getMainForm
        {
            get
            {
                if (frm == null)
                {
                    frm = new FRM_Main();
                    frm.FormClosed += new FormClosedEventHandler(frm_FormClosed);
                }
                return frm;
            }
        }
        public FRM_Main()
        {
            if (frm == null)
                frm = this;
            InitializeComponent();
            this.productsToolStripMenuItem.Enabled = false;
            this.customersToolStripMenuItem.Enabled = false;
            this.usersToolStripMenuItem.Enabled = false;
            this.makeBackupToolStripMenuItem.Enabled = false;
            this.restoreBackupToolStripMenuItem.Enabled = false;
        }

        private void restoreBackupToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void FRM_Main_Load(object sender, EventArgs e)
        {

        }

        private void signInToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PL.FRM_Login login = new PL.FRM_Login();
            login.ShowDialog();
            login.MdiParent = this;
        }

        private void signOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.productsToolStripMenuItem.Enabled = false;
            this.customersToolStripMenuItem.Enabled = false;
            this.usersToolStripMenuItem.Enabled = false;
            this.makeBackupToolStripMenuItem.Enabled = false;
            this.restoreBackupToolStripMenuItem.Enabled = false;
            
        }

        private void addNewProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PL.FRM_ADD_PRODUCTS add = new PL.FRM_ADD_PRODUCTS();
            add.ShowDialog();
            add.MdiParent = this;

        }

        private void manageProductsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FRM_PRODUCTS products = new FRM_PRODUCTS();
            products.ShowDialog();
            products.MdiParent = this;
        }

        private void manageCategoriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FRM_CATEGORIES frm = new FRM_CATEGORIES();
            frm.ShowDialog();

        }

        private void manageCusomersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PL.FRM_CUSTOMER c = new FRM_CUSTOMER();
            c.ShowDialog();
            c.MdiParent = this;
        }

        private void addNewCustomerToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void newSellToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void manageSellsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FRM_ORDERS frm = new FRM_ORDERS();
            frm.ShowDialog();
        }
    }
}
