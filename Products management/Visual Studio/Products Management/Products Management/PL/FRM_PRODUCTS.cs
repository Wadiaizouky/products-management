﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
namespace Products_Management.PL
{
    public partial class FRM_PRODUCTS : Form
    {
        private static FRM_PRODUCTS pro;
        BL.CLS_PRODUCTS product = new BL.CLS_PRODUCTS();
        public FRM_PRODUCTS()
        {
            InitializeComponent();
            if (pro == null)
                pro = this;
            this.BackColor = Color.FromArgb(45, 45, 45);

            dataGridView1.DataSource=product.Get_All_Products();
            
        }
        static void FRM_PRODUCTS_Close(object sender,FormClosedEventArgs e)
        {
            pro = null;
        }
        public static FRM_PRODUCTS getProductForm
        {
            get
            {
                if (pro == null)
                {
                    pro = new FRM_PRODUCTS();
                    pro.FormClosed += new FormClosedEventHandler(FRM_PRODUCTS_Close);
                }
                return pro;
            }
        }
        private void button8_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            RPT.rpt_all_products rpt = new RPT.rpt_all_products();
            rpt.Refresh();
            ExportOptions eo = new ExportOptions();
            DiskFileDestinationOptions dfdo = new DiskFileDestinationOptions();
            ExcelFormatOptions efo = new ExcelFormatOptions();
            dfdo.DiskFileName = @"F:\file.xls";
            eo = rpt.ExportOptions;
            eo.ExportDestinationType = ExportDestinationType.DiskFile;
            eo.ExportFormatType = ExportFormatType.Excel;
            eo.ExportFormatOptions = efo;
            eo.ExportDestinationOptions = dfdo;
            rpt.Export();

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

            FRM_ADD_PRODUCTS product1 = new FRM_ADD_PRODUCTS();
            product1.ShowDialog();
            dataGridView1.DataSource = product.Get_All_Products();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want really delete this product?", "Deleting", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
            {
                string s = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                product.DeleteProduct(s);
                MessageBox.Show("The product is deleted successfully", "Deleting", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DataTable dt = new DataTable();
                dt = product.Get_All_Products();
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("The deleting is canceled", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            FRM_ADD_PRODUCTS frm = new FRM_ADD_PRODUCTS();
            frm.txtRef.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            frm.txtDec.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            frm.txtQte.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            frm.txtPrice.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            frm.cmbCategories.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            frm.Text = "Edit Product's details : " + dataGridView1.CurrentRow.Cells[0].Value.ToString();
            frm.btnOK.Text = "Update";
            frm.state = "update";
            frm.txtRef.ReadOnly = true;
            byte[] image = (byte[])product.GET_IMAGE_PRODUCT(this.dataGridView1.CurrentRow.Cells[0].Value.ToString()).Rows[0][0];
            MemoryStream memory = new MemoryStream(image);
            frm.pBox.Image = Image.FromStream(memory);
            //dataGridView1.DataSource = product.Get_All_Products();
            frm.ShowDialog();
            
            


        }

        private void button9_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            if (comboBox1.SelectedItem.ToString()=="Product name")
            {
                
                dt=product.GET_ID1(textBox1.Text);
                dataGridView1.DataSource = dt;
            }else if(comboBox1.SelectedItem.ToString()=="Category name")
            {
                dt = product.GET_CATEGORY(textBox1.Text);
                dataGridView1.DataSource = dt;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {

            dataGridView1.DataSource = product.Get_All_Products();

            ;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PL.FRM_Preview frm = new PL.FRM_Preview();
            byte[] image = (byte[])product.GET_IMAGE_PRODUCT(dataGridView1.CurrentRow.Cells[0].Value.ToString()).Rows[0][0];
            MemoryStream memory = new MemoryStream(image);
            frm.pictureBox1.Image = Image.FromStream(memory);
            frm.ShowDialog();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            RPT.rpt_prd_single rpt = new RPT.rpt_prd_single();
            rpt.SetParameterValue("@ID", this.dataGridView1.CurrentRow.Cells[0].Value.ToString());
            RPT.FRM_RPT_PRODUCT frm = new RPT.FRM_RPT_PRODUCT();
            frm.crystalReportViewer1.ReportSource = rpt;
            frm.ShowDialog();

        }

        private void button6_Click(object sender, EventArgs e)
        {
            RPT.rpt_all_products rpt = new RPT.rpt_all_products();
            rpt.Refresh();
            RPT.FRM_RPT_PRODUCT frm = new RPT.FRM_RPT_PRODUCT();
           
            frm.crystalReportViewer1.ReportSource = rpt;
            frm.ShowDialog();
        }
    }
}
